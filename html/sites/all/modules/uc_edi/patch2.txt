Index: uc_edi_ftp/CHANGELOG.txt
===================================================================
RCS file: uc_edi_ftp/CHANGELOG.txt
diff -N uc_edi_ftp/CHANGELOG.txt
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ uc_edi_ftp/CHANGELOG.txt	13 Dec 2010 21:47:19 -0000
@@ -0,0 +1,5 @@
+$Id$
+
+Journal 6.x-1.x, 2010-12-13
+---------------------------
+by AntoineSolutions: Created the Ubercart EDI FTP sub-module.
Index: uc_edi_ftp/README.txt
===================================================================
RCS file: uc_edi_ftp/README.txt
diff -N uc_edi_ftp/README.txt
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ uc_edi_ftp/README.txt	13 Dec 2010 21:47:19 -0000
@@ -0,0 +1,45 @@
+$Id$
+
+-- SUMMARY --
+
+The Ubercart EDI FTP module provides an FTP delivery method for the Ubercart
+EDI module.
+
+
+-- REQUIREMENTS --
+
+* Ubercart EDI: http://drupal.org/project/uc_edi
+
+
+-- INSTALLATION --
+
+* Install as usual, see http://drupal.org/node/70151 for further information.
+
+
+-- CONFIGURATION --
+
+* Resources
+  - Export Delivery Methods settings page:
+    admin/store/settings/edi/export/delivery-methods
+
+* Description
+  The Ubercart EDI FTP module provides an FTP Export Delivery Method. The user
+  must provide valid FTP account credentials to use for uploading to an FTP
+  server.
+
+
+-- CONTACT --
+
+Current maintainers:
+* Jon Antoine (AntoineSolutions) - http://drupal.org/user/192192
+
+This project has been sponsored by:
+* Antoine Solutions
+  Specializing in Drupal powered sites, Antoine Solutions offers Design,
+  Development, Search Engine Optimisation (SEO) and Search Engine Marketing
+  (SEM). Visit http://www.antoinesolutions.com for more information.
+
+* Showers Pass
+  Technically engineered cycling gear for racers, commuters, messengers and
+  everyday cycling enthusiasts. Visit http://www.showerspass.com for more
+  information.
Index: uc_edi_ftp/uc_edi_ftp.info
===================================================================
RCS file: uc_edi_ftp/uc_edi_ftp.info
diff -N uc_edi_ftp/uc_edi_ftp.info
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ uc_edi_ftp/uc_edi_ftp.info	13 Dec 2010 21:47:19 -0000
@@ -0,0 +1,6 @@
+; $Id$
+name = Ubercart EDI FTP
+description = Provides an FTP export delivery method for the Ubercart EDI module.
+dependencies[] = uc_edi
+package = Ubercart - EDI
+core = 6.x
Index: uc_edi_ftp/uc_edi_ftp.install
===================================================================
RCS file: uc_edi_ftp/uc_edi_ftp.install
diff -N uc_edi_ftp/uc_edi_ftp.install
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ uc_edi_ftp/uc_edi_ftp.install	13 Dec 2010 21:47:19 -0000
@@ -0,0 +1,19 @@
+<?php
+// $Id$
+
+/**
+ * @file
+ * Install, update and uninstall functions for the Ubercart EDI FTP module.
+ */
+
+/**
+ * Implements hook_uninstall().
+ */
+function uc_edi_ftp_uninstall() {
+  // Delete module variables.
+  variable_del('uc_edi_export_delivery_method_ftp');
+  variable_del('uc_edi_ftp_host');
+  variable_del('uc_edi_ftp_port');
+  variable_del('uc_edi_ftp_user');
+  variable_del('uc_edi_ftp_pass');
+}
Index: uc_edi_ftp/uc_edi_ftp.module
===================================================================
RCS file: uc_edi_ftp/uc_edi_ftp.module
diff -N uc_edi_ftp/uc_edi_ftp.module
--- /dev/null	1 Jan 1970 00:00:00 -0000
+++ uc_edi_ftp/uc_edi_ftp.module	13 Dec 2010 21:47:19 -0000
@@ -0,0 +1,206 @@
+<?php
+// $Id$
+
+/**
+ * @file
+ * Defines an FTP export delivery method.
+ */
+
+/******************************************************************************
+ * Hook Functions (Drupal)
+ *****************************************************************************/
+
+/**
+ * Implements hook_help().
+ */
+function uc_edi_ftp_help($path, $arg) {
+  switch ($path) {
+    case 'admin/help#uc_edi_ftp':
+      $output = '';
+      $output .= '<h2>' . t('Resources') . '</h2>';
+      $output .= '<ul><li>' . t('Export Delivery Methods settings page: <a href="!url">admin/store/settings/edi/export/delivery-methods</a>', array('!url' => url('admin/store/settings/edi/export/delivery-methods'))) . '</li></ul>';
+      $output .= '<h2>' . t('Description') . '</h2>';
+      $output .= '<p>' . t('The Ubercare EDI FTP module provides an FTP Export Delivery Method. The user must provide valid FTP account credentials to use for uploading to an FTP server.') . '</p>';
+
+      return $output;
+  }
+}
+
+/**
+ * Implements hook_form_FORM_ID_alter().
+ */
+function uc_edi_ftp_form_uc_edi_export_delivery_methods_form_alter(&$form, &$form_state) {
+  $form['#validate'][] = 'uc_edi_ftp_export_delivery_method_form_validate';
+}
+
+/*******************************************************************************
+ * Hook Functions (Ubercart)
+ ******************************************************************************/
+
+/**
+ * Implementation of hook_uc_edi_export_delivery_methods().
+ */
+function uc_edi_ftp_uc_edi_export_delivery_methods() {
+  $methods[] = array(
+    'id' => 'ftp',
+    'title' => t('FTP'),
+    'description' => t('Deliver export files to an FTP server.'),
+    'callbacks' => array(
+      'help' => 'uc_edi_ftp_export_delivery_method_help',
+      'settings' => 'uc_edi_ftp_export_delivery_method_form',
+      'submit' => 'uc_edi_ftp_export_delivery_method_submit',
+    ),
+  );
+
+  return $methods;
+}
+
+/******************************************************************************
+ * Module and Helper Functions
+ *****************************************************************************/
+
+/**
+ * Callback function for the FTP export delivery method help.
+ *
+ * @return
+ *   HTML link to the FTP delivery method help page followed by a short
+ *   description.
+ */
+function uc_edi_ftp_export_delivery_method_help() {
+  return t('<a href="!url">FTP</a>: Deliver export files to an FTP server.', array('!url' => url('admin/help/uc_edi_ftp')));
+}
+
+/**
+ * Callback function for the FTP export delivery method form.
+ *
+ * @return
+ *   An un-rendered settings form for the FTP export delivery method.
+ */
+function uc_edi_ftp_export_delivery_method_form() {
+  $form['ftp']['uc_edi_ftp_host'] = array(
+    '#type' => 'textfield',
+    '#title' => t('Host'),
+    '#description' => t("The FTP server address. This parameter shouldn't have any trailing slashes and shouldn't be prefixed with ftp://."),
+    '#default_value' => variable_get('uc_edi_ftp_host', ''),
+    '#required' => TRUE,
+  );
+  $form['ftp']['uc_edi_ftp_port'] = array(
+    '#type' => 'textfield',
+    '#title' => t('Port'),
+    '#description' => t('If omitted, the default FTP port (21) will be used.'),
+    '#maxlength' => 5,
+    '#size' => 5,
+    '#default_value' => variable_get('uc_edi_ftp_port', '21'),
+  );
+  $form['ftp']['uc_edi_ftp_user'] = array(
+    '#type' => 'textfield',
+    '#title' => t('Username'),
+    '#default_value' => variable_get('uc_edi_ftp_user', ''),
+    '#required' => TRUE,
+  );
+  $form['ftp']['uc_edi_ftp_pass'] = array(
+    '#type' => 'password',
+    '#title' => t('Password'),
+    '#description' => t('The FTP account password.'),
+    '#default_value' => variable_get('uc_edi_ftp_pass', ''),
+    '#required' => TRUE,
+  );
+
+  return $form;
+}
+
+/**
+ * Handles validation of the Ubercart EDI e-mail export delivery method form.
+ *
+ * @see uc_edi_ftp_export_delivery_method_form()
+ */
+function uc_edi_ftp_export_delivery_method_form_validate($form, &$form_state) {
+  if ($form_state['values']['uc_edi_export_delivery_method_ftp']) {
+    $host = $form_state['values']['uc_edi_ftp_host'];
+    $port = $form_state['values']['uc_edi_ftp_port'];
+    $user = $form_state['values']['uc_edi_ftp_user'];
+    $pass = $form_state['values']['uc_edi_ftp_pass'];
+
+    // If there are no errors so far.
+    if(!form_get_errors()) {
+      // Log into the ftp server.
+      $ftp = uc_edi_ftp_login($host, $port, $user, $pass);
+
+      // If the connection failed.
+      if (!$ftp) {
+        form_set_error('ftp', t('Failed to authenticate.'));
+      }
+      else {
+        // Close the FTP connection.
+        @ftp_close($ftp);
+      }
+    }
+  }
+}
+
+/**
+ * Log in to an FTP server.
+ *
+ * @param $host
+ *   The FTP server address.
+ * @param $port
+ *   The FTP connection port.
+ * @param $user
+ *   The FTP username.
+ * @param $pass
+ *   The FTP password.
+ *
+ * @return
+ *   Valid FTP object.
+ */
+function uc_edi_ftp_login($host, $port, $user, $pass) {
+  // Open an FTP connection with the server.
+  if ($ftp = @ftp_connect($host, $port)) {
+    // Log onto the FTP server.
+    if (@ftp_login($ftp, $user, $pass)) {
+      // Enable passive mode.
+      @ftp_pasv($ftp, TRUE);
+
+      return $ftp;
+    }
+    else {
+      @ftp_close($ftp);
+    }
+  }
+
+  return FALSE;
+}
+
+/**
+ * Callback function for the FTP export delivery method submission.
+ *
+ * @return
+ *   TRUE if the file was submitted successfully, otherwise false.
+ */
+function uc_edi_ftp_export_delivery_method_submit($filepath, $filename) {
+  // Get FTP credentials.
+  $host = variable_get('uc_edi_ftp_host', '');
+  $port = variable_get('uc_edi_ftp_port', 21);
+  $user = variable_get('uc_edi_ftp_user', '');
+  $pass = variable_get('uc_edi_ftp_pass', '');
+
+  // Log into the FTP server.
+  if ($ftp = uc_edi_ftp_login($host, $port, $user, $pass)) {
+    // Send the file to the server.
+    if (@ftp_put($ftp, $filename, $filepath, FTP_ASCII)) {
+      // Close the FTP connection.
+      @ftp_close($ftp);
+
+      watchdog('uc_edi_ftp', t('EDI export saved to the FTP server.'));
+      return TRUE;
+    }
+    else {
+      watchdog('uc_edi_ftp', 'Problem saving the file to the FTP server. Please verify the account has write permissions.');
+    }
+  }
+  else {
+    watchdog('uc_edi_ftp', 'Problem loging into the FTP server. Check your FTP export delivery method settings and the FTP server settings.', array(), WATCHDOG_ERROR);
+  }
+
+  return FALSE;
+}
