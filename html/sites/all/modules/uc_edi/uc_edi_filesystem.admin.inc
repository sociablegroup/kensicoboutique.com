<?php
// $Id$

/**
 * @file
 * Ubercart EDI filesystem administration and module settings UI.
 */

/**
 * Creates a table with links for downloading and archiving export files.
 *
 * @return string
 *   An HTML table of order export files to be downloaded or archived if order
 *   export files were found, otherwise a default message explaining that no
 *   order export files were found.
 */
function uc_edi_filesystem_order_export_files() {
  $output = '';

  $files = file_scan_directory(variable_get('uc_edi_filesystem_export_dir', ''), '.*\\.' . variable_get('uc_edi_order_export_extension', 'edi'), array('.', '..', 'CVS'), 0, FALSE);
  $rows = array();
  foreach ($files as $file) {
    $rows[] = array($file->basename, t('<a href="!url">Download</a>', array('!url' => url('admin/store/edi/orders/export/files/download/' . $file->basename))), t('<a href="!url">Archive</a>', array('!url' => url('admin/store/edi/orders/export/files/archive/' . $file->basename))));
  }
  $header = array(t('Order export file'), array('data' => t('Actions'), 'colspan' => 2));
  if (count($rows)) {
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No order EDI export files were found in the export directory.');
  }

  return $output;
}

/**
 * Allows an order export file to be downloaded.
 *
 * @param $filename
 *   The name of the order export file to be downloaded.
 */
function uc_edi_filesystem_order_export_files_download($filename = '') {
  $file = rtrim(variable_get('uc_edi_filesystem_export_dir', ''), '/\\') . '/' . $filename;

  if (file_exists($file)) {
    $data = fopen($file, 'rb');
    ob_end_clean();

    $headers = array(
      'Content-Length:' . filesize($file),
      'Content-Disposition: attachment; filename="' . basename($file) . '"',
      'Content-Type: application/octet-stream'
    );
    foreach ($headers as $header) {
      $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
      drupal_set_header($header);
    }

    while (!feof($data)) {
      print fread($data, 1024);
    }
    fclose($data);
    exit();
  }
  else {
    drupal_not_found();
  }
}

/**
 * Archives an order export file.
 *
 * @param $filename
 *   The name of the order export file to be archived.
 */
function uc_edi_filesystem_order_export_files_archive($filename = '') {
  if (@rename(rtrim(variable_get('uc_edi_filesystem_export_dir', ''), '/\\') . '/' . $filename, rtrim(variable_get('uc_edi_filesystem_export_archive_dir', ''), '/\\') . '/' . $filename)) {
    drupal_set_message(t('The selected order export file %file was archived successfully.', array('%file' => $filename)));
  }
  else {
    drupal_set_message(t('Problem archiving the order export file. Check your EDI settings and directory permissions.'), 'error');
  }

  drupal_goto('admin/store/edi/orders/export/files');
}
