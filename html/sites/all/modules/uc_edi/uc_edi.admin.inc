<?php
// $Id: uc_edi.admin.inc,v 1.2 2010/11/24 16:37:17 antoinesolutions Exp $

/**
 * @file
 * Ubercart EDI administration and module settings UI.
 */

/**
 * Form builder for the EDI import settings form.
 *
 * @see uc_edi_settings_import_form_validate()
 * @ingroup forms.
 */
function uc_edi_settings_import_form() {
  if ((trim(variable_get('uc_edi_order_import_dir', '')) == '' ||
       trim(variable_get('uc_edi_order_import_archive_dir', '')) == '') &&
       empty($_POST['form_id'])) {
    drupal_set_message(t('One or more required directory fields need to be filled out. It is recommended that you use a directory outside of your document root for improved security. You may specify an absolute path on the server or a relative path from the Drupal directory.'), 'error');
  }

  $form['uc_edi_order_import_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Import directory'),
    '#description' => t('The directory path to search for data files ready for import. Omit the trailing slash.'),
    '#default_value' => variable_get('uc_edi_order_import_dir', ''),
  );
  $form['uc_edi_order_import_archive_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Import archive directory'),
    '#description' => t('The directory path to move processed data files to for storage. Omit the trailing slash.'),
    '#default_value' => variable_get('uc_edi_order_import_archive_dir', ''),
  );
  $form['uc_edi_order_import_extension'] = array(
    '#type' => 'textfield',
    '#title' => t('Import file extension'),
    '#description' => t('Enter the file extension expected for order import files.'),
    '#default_value' => variable_get('uc_edi_order_import_extension', 'edi'),
    '#maxlength' => 6,
  );
  $form['uc_edi_order_import_delimiter'] = array(
    '#type' => 'select',
    '#title' => t('Import pattern delimiter'),
    '#options' => array(
      ',' => t('comma (,)'),
      '|' => t('pipe (|)'),
      '\t' => t('tab (\t)'),
    ),
    '#default_value' => variable_get('uc_edi_order_import_delimiter', '\t'),
  );
  $form['uc_edi_order_import_freq'] = array(
    '#type' => 'select',
    '#title' => t('Import frequency'),
    '#description' => t('The frequency at which automatic imports should occur.'),
    '#options' => array(
      '1 second' => t('Every cron run'),
      '1 hour' => t('Once an hour'),
      '6 hours' => t('Every six hours'),
      '12 hours' => t('Every twelve hours'),
      '1 day' => t('Once a day'),
      'never' => t('Never'),
    ),
    '#default_value' => variable_get('uc_edi_order_import_freq', '1 second'),
  );
  foreach (uc_order_status_list('general') as $status) {
    $options[$status['id']] = $status['title'];
  }
  $form['uc_edi_order_imported_status'] = array(
    '#type' => 'select',
    '#title' => t('Imported order status'),
    '#description' => t('Select a status for orders to be moved to when a response is received for that order.'),
    '#options' => $options,
    '#default_value' => variable_get('uc_edi_order_imported_status', 'completed'),
  );
  $form['uc_edi_order_import_rules'] = array(
    '#type' => 'textarea',
    '#title' => t('Order import rules'),
    '#description' => t('Order import rules have set patterns and keywords. <a href="!url">Click here</a> for help.', array('!url' => url('admin/help/uc_edi'))),
    '#default_value' => variable_get('uc_edi_order_import_rules', 'ORD|id:2|us|oc:Your order has shipped via #3 with tracking number #4.'),
  );

  $form = system_settings_form($form);

  return $form;
}

/**
 * Handels validation of the EDI import settings form.
 *
 * @see uc_edi_settings_import_form()
 */
function uc_edi_settings_import_form_validate($form, &$form_state) {
  if (trim($form_state['values']['uc_edi_order_import_dir']) != '' &&
      !is_dir($form_state['values']['uc_edi_order_import_dir'])) {
    form_set_error('uc_edi_order_import_dir', t('You have specified a non-existent order import directory.'));
  }
  if (trim($form_state['values']['uc_edi_order_import_archive_dir']) != '' &&
      !is_dir($form_state['values']['uc_edi_order_import_archive_dir'])) {
    form_set_error('uc_edi_order_import_archive_dir', t('You have specified a non-existent order import archive directory.'));
  }
}

/**
 * Form builder for the EDI export settings form.
 *
 * @see uc_edi_settings_export_form_validate()
 * @ingroup forms.
 */
function uc_edi_settings_export_form() {
  $form['filesystem']['uc_edi_order_export_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Export file prefix'),
    '#description' => t('Enter a prefix to use for your exported order files. Leave blank for none.'),
    '#default_value' => variable_get('uc_edi_order_export_prefix', 'export-'),
    '#maxlength' => 10,
  );
  $form['filesystem']['uc_edi_order_export_extension'] = array(
    '#type' => 'textfield',
    '#title' => t('Export file extension'),
    '#description' => t('Enter the file extension to use for your exported order files.'),
    '#default_value' => variable_get('uc_edi_order_export_extension', 'edi'),
    '#maxlength' => 6,
  );
  $form['uc_edi_order_export_method'] = array(
    '#type' => 'radios',
    '#title' => t('Export method'),
    '#description' => t('The method used to export the order.'),
    '#options' => array(
      'checkout' => t('On checkout completion'),
      'cron' => t('At a specified interval'),
      'manual' => t('Manualy'),
    ),
    '#default_value' => variable_get('uc_edi_order_export_method', 'cron'),
    '#ahah' => array(
      'method' => 'replace',
      'path' => 'admin/store/edi/export/method-js',
      'wrapper' => 'method-sub-wrapper',
    ),
  );
  $form['uc_edi_order_export_method_sub'] = uc_edi_order_export_method();
  $form['uc_edi_order_export_template'] = array(
    '#type' => 'select',
    '#title' => t('Export template'),
    '#description' => t('Select the export template to use.<br />This is separate from the template used when exporting on checkout completion which is configured through <a href="!url">Conditional actions</a>.', array('!url' => url(CA_UI_PATH))),
    '#options' => uc_edi_template_options(),
    '#default_value' => variable_get('uc_edi_order_export_template', 'default'),
  );
  $form['uc_edi_order_export_next_batch_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Next order batch ID'),
    '#description' => t('<b>DISABLED:</b> Adjusting this value may result in duplicate batch IDs. Adjustments must be made in the database.<br />The batch ID is incremented for every export and useful for tracking.'),
    '#default_value' => variable_get('uc_edi_order_export_next_batch_id', 1000),
    '#disabled' => TRUE,
  );

  $form = system_settings_form($form);

  // We will call system_settings_form_submit() manually, so remove it for now.
  unset($form['#submit']);
  return $form;
}

/**
 * Returns the unrendered method sub form.
 */
function uc_edi_order_export_method() {
  // Retrieve variable values from $_POST or use the default.
  $method = $_POST['uc_edi_order_export_method'] ? $_POST['uc_edi_order_export_method'] : variable_get('uc_edi_order_export_method', 'cron');
  $frequency = $_POST['uc_edi_order_export_freq'] ? $_POST['uc_edi_order_export_freq'] : variable_get('uc_edi_order_export_freq', '1 day');
  $status = $_POST['uc_edi_order_export_status'] ? $_POST['uc_edi_order_export_status'] : variable_get('uc_edi_order_export_status', 'edi_export');

  // If the method has changed, alert the user to conditional action changnes.
  if ($method != variable_get('uc_edi_order_export_method', 'cron')) {
    if ($method == 'checkout') {
      drupal_set_message(t('The <a href="!url1">Export the order</a> predicate will be enabled. Please verify your <a href="!url2">Conditional Actions configuration</a>.', array('!url1' => url('admin/store/ca/uc_edi_order_export/edit'), '!url2' => url('admin/store/ca'))), 'warning');
    }
    elseif (variable_get('uc_edi_order_export_method', 'cron') == 'checkout') {
      drupal_set_message(t('The <a href="!url1">Export the order</a> predicate will be disabled. Please verify your <a href="!url2">Conditional Actions configuration</a>.', array('!url1' => url('admin/store/ca/uc_edi_order_export/edit'), '!url2' => url('admin/store/ca'))), 'warning');
    }
  }

  // Create an array of order status options.
  foreach (uc_order_status_list('general') as $uc_status) {
    $options[$uc_status['id']] = $uc_status['title'];
  }

  // Build the default form.
  $form['uc_edi_order_export_freq'] = array(
    '#type' => 'select',
    '#title' => t('Export frequency'),
    '#description' => t('The frequency at which automatic exports should occur.'),
    '#options' => array(
      '1 second' => t('Every cron run'),
      '1 hour' => t('Once an hour'),
      '6 hours' => t('Every six hours'),
      '12 hours' => t('Every twelve hours'),
      '1 day' => t('Once a day'),
    ),
    '#default_value' => $frequency,
  );
  $form['uc_edi_order_export_status'] = array(
    '#type' => 'select',
    '#title' => t('Ready for export order status'),
    '#description' => t('Select a status used to mark orders as ready for export.'),
    '#options' => $options,
    '#default_value' => $status,
    '#ahah' => array(
      'event' => 'change',
      'method' => 'replace',
      'path' => 'admin/store/edi/export/method-js',
      'wrapper' => 'method-sub-wrapper',
    ),
  );

  // Generate the correct form based on the method.
  switch ($method) {
    // Export on checkout completion.
    case 'checkout':
      // Display help message to user.
      $form['message'] = array(
        '#value' => t('Exporting orders on checkout completion uses the <a href="!url1">Export the order</a> predicate for exporting orders. Please verify your <a href="!url2">Conditional Actions settings</a>.', array('!url1' => url('admin/store/ca/uc_edi_order_export/edit'), '!url2' => url('admin/store/ca'))),
      );
      // Hide unused form items.
      $form['uc_edi_order_export_freq']['#type'] = 'hidden';
      $form['uc_edi_order_export_status']['#type'] = 'hidden';
      break;

    // Export at a specified interval.
    case 'cron':
      // Nothing to do.
      break;

    // Export manually.
    case 'manual':
      // Display help message to user.
      $form['message'] = array(
        '#value' => t('Exporting orders manually can be accomplished using the <a href="!url">Manual EDI import/export form</a>.', array('!url' => url('admin/store/edi'))),
      );
      // Hide unused form items.
      $form['uc_edi_order_export_freq']['#type'] = 'hidden';
      break;
  }

  // Validate Conditional Actions is configured correctly for the specified
  // order export status.
  if (!uc_edi_order_export_status_validate($method, $status)) {
    form_set_error('uc_edi_order_export_status', t('A predicate must exist that sets the order status to that which is defined by the <strong>Ready for export order status</strong> field. See the <a href="!url">Export Configuration documentation</a>.', array('!url' => url('admin/help/uc_edi', array('fragment' => 'export-configuration')))));
  }

  // Add prefix and suffix elements.
  $form += array(
    '#prefix' => '<div id="method-sub-wrapper">',
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Returns the rendered method sub form.
 */
function uc_edi_order_export_method_js() {
  // Retrieve the unrendered frequency form.
  $form = uc_edi_order_export_method();

  // Remove #prefix and #suffix to avoid a duplicate wrapper.
  unset($form['#prefix'], $form['#suffix']);

  // Build and render the form, then return it in JSON format.
  $form_state = array();
  $form = form_builder($form['#name'], $form, $form_state);
  $output = theme('status_messages', 'error');
  $output .= theme('status_messages', 'warning');
  $output .= drupal_render($form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Verify Conditional Actions is configured to work with Ubercart EDI.
 *
 * @param $method
 *   An export method.
 * @param $status
 *   An order status.
 *
 * @return boolean
 *   Returns FALSE if the $method requires that a Conditional Actions predicate
 *   set an orders status to $status and one does not exist, otherwise TRUE.
 */
function uc_edi_order_export_status_validate($method, $status) {
  // If orders are not exported at checkout.
  if ($method != 'checkout') {
    // Load all the module defined predicates into a temporary array.
    $temp = module_invoke_all('ca_predicate');

    // Assign a default weight if need be.
    foreach ($temp as $key => $value) {
      $temp[$key]['#pid'] = $key;
      if (!isset($value['#weight'])) {
        $temp[$key]['#weight'] = 0;
      }
    }

    // Load and loop through all the database stored predicates.
    $result = db_query("SELECT * FROM {ca_predicates}");
    while ($predicate = db_fetch_array($result)) {
      // Prepare the database data into a predicate.
      $predicate = ca_prepare_db_predicate($predicate);

      // Overrides the module defined predicate if it's been modified by a user.
      $temp[$predicate['#pid']] = $predicate;
    }

    usort($temp, 'ca_weight_sort');

    // Copy the temporary array of predicates into a keyed array.
    $predicates = array();
    foreach ($temp as $predicate) {
      $predicates[$predicate['#pid']] = $predicate;
    }

    // Load and loop through all the database stored predicates.
    foreach ($predicates as $predicate) {
      // Loop through all predicate actions.
      foreach ($predicate['#actions'] as $action) {
        // Return true is a predicate is found that sets an orders status to
        // the status passed in.
        if ($action['#settings']['order_status'] == $status) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  return TRUE;
}

/**
 * Handler for submitting the export settings form.
 */
function uc_edi_settings_export_form_submit($form, &$form_state) {
  // Call the default submit handler first.
  system_settings_form_submit($form, $form_state);

  // If the export method has changed from or to 'checkout'.
  if ($form['uc_edi_order_export_method']['#default_value'] != $form['uc_edi_order_export_method']['#value'] &&
     ($form['uc_edi_order_export_method']['#default_value'] == 'checkout' || $form['uc_edi_order_export_method']['#value'] == 'checkout')) {
    // Load the order export predicate.
    $predicate = ca_load_predicate('uc_edi_order_export');
    $update = FALSE;

    // Update the 'Export the order' predicate based on export method.
    if ($form_state['values']['uc_edi_order_export_method'] == 'checkout') {
      if ($predicate['#status'] == 0) {
        $predicate['#status'] = 1;
        $update = TRUE;
      }
    }
    else {
      if ($predicate['#status'] == 1) {
        $predicate['#status'] = 0;
        $update = TRUE;
      }
    }

    // If manual predicate update was required.
    if ($update) {
      ca_save_predicate($predicate);
    }

    // Inform the user that the predicate was updated.
    drupal_set_message(t('The <a href="!url1">Export the order</a> predicate has been updated. Please verify your <a href="!url2">Conditional Actions settings</a>.', array('!url1' => url('admin/store/ca/uc_edi_order_export/edit'), '!url2' => url('admin/store/ca'))), 'warning');
  }
}

/**
 * Form builder for the EDI export delivery methods settings form.
 *
 * @see uc_edi_export_delivery_methods_form_validate()
 * @ingroup forms.
 */
function uc_edi_export_delivery_methods_form() {
  $methods = uc_edi_export_delivery_method_list();

  $form['methods_info'] = array(
    '#value' => t('The settings forms below are for the export delivery methods defined by enabled modules. Click a name to expand its options and adjust the settings accordingly.') .'</div><br />',
  );

  foreach ($methods as $method) {
    $form[$method['id']] = array(
      '#type' => 'fieldset',
      '#title' => $method['title'],
      '#description' => $method['description'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form[$method['id']]['uc_edi_export_delivery_method_' . $method['id']] = array(
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => variable_get('uc_edi_export_delivery_method_' . $method['id'], FALSE),
    );
    $form = array_merge_recursive($form, $method['callbacks']['settings']('settings'));
  }

  return system_settings_form($form);
}

/**
 * Form builder for the order import manual form.
 *
 * @see uc_edi_order_import_manual_form_submit()
 * @see uc_edi_order_import_manual_form_reset()
 * @ingroup forms
 */
function uc_edi_order_import_manual_form() {
  // Grab all valid files in the import directory.
  $files = uc_edi_get_import_files(variable_get('uc_edi_order_import_dir', ''), variable_get('uc_edi_order_import_extension', 'edi'));

  // Figure out how many seconds remain till the next import will run.
  if (variable_get('uc_edi_order_import_freq', '1 second') != 'never') {
    $elapsed = time() - variable_get('uc_edi_order_import_last', 0);
    $needed = time() - strtotime(variable_get('uc_edi_order_import_freq', '1 second') . ' ago');
    $seconds = $needed - $elapsed;
  }
  else {
    $seconds = t('(auto import disabled) 0');
  }

  $form['import_info'] = array(
    '#value' => '<div><b>' . t('There are !count files ready to import in !seconds seconds:', array('!count' => count($files), '!seconds' => $seconds < 0 ? 0 : $seconds)) . '</b><br />' . implode(', ', $files) . '<p></p></div>',
  );
  $form['import_now'] = array(
    '#type' => 'submit',
    '#submit' => array('uc_edi_order_import_manual_form_submit'),
    '#value' => t('Import orders now'),
  );
  $form['reset_import'] = array(
    '#type' => 'submit',
    '#submit' => array('uc_edi_order_import_manual_form_reset'),
    '#value' => t('Reset import timer'),
  );

  return $form;
}

/**
 * Handles manual order import submissions.
 *
 * @see uc_edi_order_import_manual_form()
 */
function uc_edi_order_import_manual_form_submit($form, &$form_state) {
  uc_edi_import_orders();
  drupal_set_message(t('Order import files have been processed.'));
}

/**
 * Handles resetting the order import timer.
 *
 * @see uc_edi_order_import_manual_form()
 */
function uc_edi_order_import_manual_form_reset($form, &$form_state) {
  variable_set('uc_edi_order_import_last', time());
  drupal_set_message(t('Order import timer reset.'));
}

/**
 * Form builder for the order export manual form.
 *
 * @see uc_edi_order_export_manual_form_submit()
 * @see uc_edi_order_export_manual_form_reset()
 * @ingroup forms
 */
function uc_edi_order_export_manual_form() {
  // Retrieve orders ready for export from database.
  $result = db_query("SELECT order_id FROM {uc_orders} WHERE order_status = '%s' ORDER BY order_id", variable_get('uc_edi_order_export_status', 'edi_export'));
  $orders = array();
  $count = db_affected_rows();
  $header = TRUE;
  $footer = FALSE;

  // Build the export preview.
  for ($i = 0; $order = db_fetch_object($result); ++$i) {
    $orders[] = $order->order_id;
    // Load the order object.
    $order = uc_order_load($order->order_id);

    if ($i + 1 == $count) {
      $footer = TRUE;
    }

    // Create the order export string.
    $preview .= theme('uc_edi', $order, variable_get('uc_edi_order_export_template', 'default'), $header, $footer);

    if ($header) {
      $header = FALSE;
    }
  }

  switch (variable_get('uc_edi_order_export_method', 'cron')) {
    case 'checkout':
      $message = t('Orders are configured to export on checkout completion. !count order(s) require manual export.', array('!count' => $count));
      break;

    case 'cron':
      // Figure out how many seconds remain till the next export will run.
      $elapsed = time() - variable_get('uc_edi_order_export_last', 0);
      $needed = time() - strtotime(variable_get('uc_edi_order_export_freq', '1 day') . ' ago');
      $seconds = $needed - $elapsed;
      $message = t('Auto export configured to export !count orders in !seconds seconds:', array('!count' => $count, '!seconds' => $seconds < 0 ? 0 : $seconds));
      break;

    case 'manual':
      $message = t('!count order(s) ready for manual export.', array('!count' => $count));
      break;
  }

  $form['export_info'] = array(
    '#value' => '<div><b>' . $message . '</b><br />' . implode(', ', $orders) . '<p></p></div>',
  );

  // Display a fieldset with a preview of the export file.
  $form['export_preview'] = array(
    '#type' => 'fieldset',
    '#title' => t('Order export preview'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  if ($count == 0) {
    $preview = t('No orders awaiting export.');
  }
  elseif (empty($preview)) {
    $preview = t('No output was generated. Check your template files.');
  }
  $form['export_preview']['contents'] = array(
    '#value' => '<div><pre>' . check_plain($preview) . '</pre></div>',
  );

  $form['export_now'] = array(
    '#type' => 'submit',
    '#submit' => array('uc_edi_order_export_manual_form_submit'),
    '#value' => t('Export orders now'),
  );
  $form['reset_export'] = array(
    '#type' => 'submit',
    '#submit' => array('uc_edi_order_export_manual_form_reset'),
    '#value' => t('Reset export timer'),
  );

  return $form;
}

/**
 * Handles manual order export submissions.
 *
 * @see uc_edi_order_export_manual_form()
 */
function uc_edi_order_export_manual_form_submit($form, &$form_state) {
  // Export orders.
   $filename = uc_edi_export_orders();

  // If the export was successfull.
  if ($filename) {
    // Inform the user of a successfull export.
    drupal_set_message(t('Order export file !file created.', array('!file' => $filename)));
  }
  else {
    // Inform the user of an unsuccessfull export.
    drupal_set_message(t('Problem creating the order export file. Check your EDI settings and directory permissions.'), 'error');
  }
}

/**
 * Handles resetting the order export timer.
 *
 * @see uc_edi_order_export_manual_form()
 */
function uc_edi_order_export_manual_form_reset($form, &$form_state) {
  variable_set('uc_edi_order_export_last', time());
  drupal_set_message(t('Order export timer reset.'));
}

/**
 * Generates a manual order export form for individual orders.
 */
function uc_edi_order_export_form($form_state, $order) {
  drupal_set_message(t('This order export form does not update the order status and should only be used for testing template files or viewing and saving already exported orders. To manually download export files, the <a href="!url">Manual import/export form</a> should be used.', array('!url' => url('admin/store/edi'))), 'warning');

  $template = variable_get('uc_edi_order_export_template', 'default');
  $form['template'] = array(
    '#type' => 'select',
    '#title' => t('Template'),
    '#description' => t('Select the export template to use.'),
    '#options' => uc_edi_template_options(),
    '#default_value' => $template,
    '#ahah' => array(
      'method' => 'replace',
      'path' => 'admin/store/orders/' . $order->order_id . '/export/template-js',
      'wrapper' => 'template-sub-wrapper',
    ),
  );
  $form['export'] = array(
    '#type' => 'fieldset',
    '#title' => t('Export'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['export']['template_sub'] = uc_edi_order_export_template($order, $template);
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Returns the unrendered order export form item.
 *
 * @param $order
 *   A reference to an order object to export.
 * @param $template
 *   A reference to an order export template name.
 *
 *  @return
 *    An array containing the unrendered form item.
 */
function uc_edi_order_export_template(&$order, &$template) {
  // Create the order export string.
  $form = array(
    '#value' => check_plain(theme('uc_edi', $order, $template, TRUE, TRUE)),
  );

  // Add prefix and suffix elements.
  $form += array(
    '#prefix' => '<pre id="template-sub-wrapper">',
    '#suffix' => '</pre>',
  );

  return $form;
}

/**
 * Returns the rendered order export form item.
 *
 * @param $order
 *   A reference to an order object to export.
 */
function uc_edi_order_export_template_js(&$order) {
  // Retrieve the template from $_POST.
  $template = $_POST['template'];

  // Retrieve the unrendered order export form item.
  $form = uc_edi_order_export_template($order, $template);

  // Remove #prefix and #suffix to avoid a duplicate wrapper.
  unset($form['#prefix'], $form['#suffix']);

  // Build and render the form item, then return it in JSON format.
  $form_state = array();
  $form = form_builder($form['#name'], $form, $form_state);
  $output = drupal_render($form);
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Downloads an individual order export.
 */
function uc_edi_order_export_form_submit($form, &$form_state) {
  $order = $form['#parameters'][2];

  // Build the http headers necessary for a file download.
  $prefix = variable_get('uc_edi_order_export_prefix', 'export-');
  $ext = variable_get('uc_edi_order_export_extension', 'edi');
  $headers = array(
    'Content-Length:' . filesize($output),
    'Content-Disposition: attachment; filename="' . $prefix . $order->order_id . '.' . $ext . '"',
    'Content-Type: application/octet-stream'
  );
  foreach ($headers as $header) {
    $header = preg_replace('/\r?\n(?!\t| )/', '', $header);
    drupal_set_header($header);
  }

  // Output the order export.
  print theme('uc_edi', $order, $form_state['values']['template'], TRUE, TRUE);
  exit();
}
