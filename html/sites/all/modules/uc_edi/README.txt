$Id: README.txt,v 1.4 2010/11/24 16:37:17 antoinesolutions Exp $

-- SUMMARY --

The Ubercart EDI module provides some simple EDI order export and update
functionality for Ubercart orders. This is useful for stores working with
fulfillment houses where orders are passed back and forth in some predetermined
format for shipping. The current module assumes some sort of FTP based system
and will handle the archiving of import files automatically. It allows you to
setup an export pattern based on a number of order and product variables, and
it provides cron functionality so orders of a particular order status may be
automatically exported for processing.

For a full description of the module, visit the project page:
  http://drupal.org/project/uc_edi

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/uc_edi


-- REQUIREMENTS --

* uc_order: http://drupal.org/project/ubercart


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Resources
  - Conditional actions users guide:
    http://www.ubercart.org/docs/user/7657/configuring_conditional_actions
  - Conditional actions configuration page: admin/store/ca
  - Export the order predicate: admin/store/ca/uc_edi_order_export/edit
  - Update an order after export predicate:
    admin/store/ca/uc_edi_order_export_update/edit
  - Ubercart EDI settings page: admin/store/settings/edi
  - Manual order import form: admin/store/edi/orders/import
  - Manual order export form: admin/store/edi/orders/export
  - Ubercart EDI permissions: admin/user/permissions#module-uc_edi

* Export Configuration
  - Methods
    Ubercart EDI has 3 methods for exporting orders. You can export orders "On
    checkout completion", "At a specified interval" or "Manually".

    - On checkout completion
      Ubercart EDI leverages "Conditional Actions" for exporting orders on
      checkout completion by creating an "Export the order" action that can be
      added to any Conditional Actions predicate. By default, the Ubercart EDI
      module creates an "Export the order" predicate tied to the "Customer
      completes checkout" trigger that calls the "Export the order" action.
      This predicate is automatically enabled when this method is selected and
      automatically disabled when another method is selected.

      To export orders on checkout completion:
      - On the "Conditional Actions configuration" page
        - Configure the "Export the order" predicate.
      - On the "Ubercart EDI settings" page
        - Set the "Export method" to "On checkout completion".

    - At a specified interval
      Ubercart EDI leverages cron for exporting orders at a specified interval.

      To export orders at a specified interval:
      - On the "Conditional Actions configuration" page
        - Create a new or modify an existing predicate so that an orders status
          is set to the status desired just before exporting ("Order processed"
          recommended).
      - On the "Ubercart EDI setting" page
        - Set the "Export method" to "At a specified interval".
        - Set the "Export frequency" to your desired interval. For accuracy,
          cron must be configured to run at a divisor/factor of the specified
          interval.
        - Set the "Ready for export order status" to the status desired just
          before an order is to be exported ("Order processed" recommended).
          This status should match that of the predicate configured above.

    - Manually
      To export orders manually:
      - Use the "Manual order export form".

  - Template files
    EDI templates use Drupal's theme system to allow complete control over
    exported content. To customize exported content you can either override
    existing templates or define new templates.

    - Override existing templates
      Templates can be overridden using normal Drupal themeing practices, i.e.
      copy uc_edi-default.tpl.php into your theme directory. If multiple themes
      are used, the template file must be copied into each themes directory.
      *Due to the way the Drupal theme layer works, the dummy file
      (uc_edi.tpl.php) must also be copied.*

    - Create new templates
      New templates can be defined by implementing hook_uc_edi_templates(). An
      array of template names should be returned.

    - Preprocess functions
      Both methods above can take advantage of the hook_preprocess_uc_edi()
      function which lets you pass any data to all EDI templates as a variable.
      The default template_preprocess_uc_edi() function adds all correctly
      defined order tokens to the template files as variables. For a list of
      order token variables specifically added by Ubercart EDI, see the
      documentation block at the top of the default template file.

    - Testing
      To easily test new template files, an "export" tab has been added to the
      order view page. This new tab allows you to select any defined template
      and automatically updates the output to that of the newly selected
      template. A save button allows you to save an export file for testing the
      import process of your fulfillment houses.
      *This order export form does not update the order status and should only
      be used for testing template files or viewing and saving orders that have
      already been processed. For manually exporting orders, the "Manual Order
      Export Form" should be used.*

  - Delivery Methods
    Ubercart EDI offers different methods for delivering export files to
    locations. Included with Ubercart EDI are the filesystem and e-mail
    delivery methods, each implemented by its own sub-module. See a delivery
    methods help page for more information on that delivery method. Other
    modules can provide their own delivery methods by implementing the
    uc_edi_export_delivery_methods hook.

    - E-mail: Deliver export files to a specified e-mail address.
    - Filesystem: Deliver export files to the local filesystem.

  - Update an order after export
    Ubercart EDI leverages "Conditional Actions" for updating orders after a
    successful export. The Ubercart EDI module creates an "Order has been
    exported" trigger that is pulled when an order is successfully exported. By
    default, an "Update an order after export" predicate is created which is
    tied to the "Order has been exported" trigger that adds an order comment,
    an admin comment and updates the order status of orders that have been
    successfully exported.

* Import Configuration
  - You may specify as many rules as you want to process order import files.
    Rules consist of a line identifier and action separated by | characters.
    Rules should be separated by line breaks (press enter after each one).
    Multiple rules may all use the same identifier, and they will all be
    applied for any matching lines in the import file.

  - Import files are parsed line by line, and each line may be handled
    differently based on the first value in the line. This first value is the
    identifier that should be used to refer to lines of that type. Use * to
    denote a rule that applies to any line.

  - When a line is processed, it is broken apart by the delimiter you have set
    for import files. Each piece may be referred to in actions by its number in
    sequence, i.e. #1, #2, #3.

  - You may set an action to perform only if a certain value exists in the
    line. Use ?3 at the start of an action and it will only be performed if the
    line has a value in the third place. Use !3 to instead do something if the
    value does not exist. Separate as many of these conditions as you want by
    colons, i.e. ?8:!10:oc:...

  - The following commands may be used with value substitution:
    - "Specify the order ID" - id:2 (This must precede other actions to specify
      which value should be used as the order ID.)
    - "Update the order status" - us (Updates to the value specified in the
      order import settings.)
    - "Add an order comment" - oc:Comment message.
    - "Add an admin comment" - ac:Comment message.
    - "Log text to the watchdog" - wd:Watchdog message.

  - How about an example? The following rules:
    ORD|id:2|us|oc:Your order has shipped via #3 with tracking number #4.
    When applied to this line in a comma delimited import file:
    ORD,234,UPS,1Z1730398193
    Would add an order comment to the order to show the customer their tracking
    information and update the order status.

* Permissions
  - administer edi orders
    Users in roles with "administer edi orders" permissions can modify module
    settings and manually import and export orders.


-- CONTACT --

Current maintainers:
* Jon Antoine (AntoineSolutions) - http://drupal.org/user/192192

This project has been sponsored by:
* Antoine Solutions
  Specializing in Drupal powered sites, Antoine Solutions offers Design,
  Development, Search Engine Optimisation (SEO) and Search Engine Marketing
  (SEM). Visit http://www.antoinesolutions.com for more information.

* Showers Pass
  Technically engineered cycling gear for racers, commuters, messengers and
  everyday cycling enthusiasts. Visit http://www.showerspass.com for more
  information.
