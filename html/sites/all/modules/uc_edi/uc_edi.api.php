<?php
// $Id$

/**
 * @file
 * Hooks provided by Ubercart EDI.
 */

/**
 * Respond to EDI template definitions.
 *
 * This hook is invoked from uc_edi_template_list() when the Ubercart EDI
 * settings page is loaded.
 *
 * @return
 *   Array of EDI template names.
 *
 * @ingroup hooks
 */
function hook_uc_edi_templates() {
  return array('default');
}

/**
 * Respond to export delivery method definitions.
 *
 * This hook is invoked from uc_edi_export_delivery_method_list() when the
 * Ubercart EDI Delivery Methods settings page is loaded and when an export is
 * triggered.
 *
 * @return
 *   An array containing the following keys.
 *   - id: The machine readable name.
 *   - title: The human readable name.
 *   - description: A human readable description.
 *   - callbacks: An array of callbacks for the supported operations.
 *     - help: Return an HTML link to the delivery method specific help page
 *       followed by a short description.
 *     - settings: Returns an un-rendered settings form array to be added to
 *       the Ubercart EDI Export Delivery Methods settings page.
 *     - submit: Submits the export file found in $filepath for delivery.
 *
 * @ingroup hooks
 */
function hook_uc_edi_export_delivery_methods() {
  $methods[] = array(
    'id' => 'filesystem',
    'title' => t('Filesystem'),
    'description' => t('Deliver export files to the local filesystem.'),
    'callbacks' => array(
      'help' => 'uc_edi_filesystem_export_delivery_method_help',
      'settings' => 'uc_edi_filesystem_export_delivery_method_form',
      'submit' => 'uc_edi_filesystem_export_delivery_method_submit',
    ),
  );

  return $methods;
}
