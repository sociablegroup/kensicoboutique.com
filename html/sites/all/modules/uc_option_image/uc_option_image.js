// $Id$
var UCOI = UCOI || {};

/**
 * Initialize.
 */
UCOI.init = function() {
  var size = Drupal.settings.UCOI.size;   
  this.images = Drupal.settings.UCOI.images; 
  this.effect = Drupal.settings.UCOI.effect;
  this.noimage = Drupal.settings.UCOI.noimage;
  this.attributes = Drupal.settings.UCOI.attributes;
  this.defaultSize = Drupal.settings.UCOI.default_size;
  this.use_lightbox = Drupal.settings.UCOI.use_lightbox;
  this.nodeid = Drupal.settings.UCOI.nodeid;
  
  // Selects                                      
  $('.add-to-cart select.form-select').change(function(){
    if (aid = UCOI.getAID(this)){
      UCOI.switchImage(aid, this, size);  
    }
  });
  
  // Radios                                      
  $('.add-to-cart .form-radios input').click(function(){
    if (aid = UCOI.getAID(this)){
      UCOI.switchImage(aid, this, size);  
    }
  });

  //onload functions
  //$('.add-to-cart .form-radios input').onload(function(){
  //  if (aid = UCOI.getAID(this)){
  //    UCOI.switchImage(aid, this, size);
  //  }
  //});


};

/**
 * Switch an option image.
 */
UCOI.switchImage = function(aid, input, size) {
  /*var pid = $(input).parents('.node').attr('id');
  var nid = pid.replace('node-', '');             */
  var nid=this.nodeid;
  var oid = $(input).val(); 
  var image;
  /*var lightbox = $(input).parents('.content').children('div.uc-option-image-block').children('a.lightbox-processed');  */
  var lightbox = $(':not(.uc-option-image-preloaded) > div.uc-option-image-block').children('a.lightbox-processed'); 
  
  if (this.use_lightbox)
/*     image = $(input).parents('.content').children('div.uc-option-image-block').children('a.lightbox-processed').children('img.uc-option-image'); */
        image = $(':not(.uc-option-image-preloaded) > div.uc-option-image-block').children('a.lightbox-processed').children('img.uc-option-image'); 
  else
/*     image = $(input).parents('.content').children('div.uc-option-image-block').children('img.uc-option-image');*/
     image =  $(':not(.uc-option-image-preloaded) > div.uc-option-image-block').children('img.uc-option-image');


  // Make sure we have permission to switch this attribute
  if (this.attributes[aid] == 0){
    return;
  }
          
  try {          
    var images = this.images[nid][aid];

    if ((oid=="" || images[oid].derivative=="") && image){
	parentImage = image[0].parentNode;
	parentImage.removeChild(image[0]);    
    }else if (image[0] && images[oid].derivative){ 
      this.switchImageEffect(image, lightbox, images[oid]);
    } else if (image[0] == null){
       if (this.use_lightbox)
/*         parentImage = $(input).parents('.content').children('div.uc-option-image-block').children('a.lightbox-processed');*/
           parentImage =  $(':not(.uc-option-image-preloaded) > div.uc-option-image-block').children('a.lightbox-processed');	       
       else
/*         parentImage = $(input).parents('.content').children('div.uc-option-image-block');*/
            parentImage = $(':not(.uc-option-image-preloaded) > div.uc-option-image-block');

       parentImage[0].innerHTML = "<img src=\""+images[oid].derivative+"\" class=\"uc-option-image\">";
       // objectToAppend = document.createElement("img");
       // objectToAppend.setAttribute("src",images[oid].derivative);
       // objectToAppend.setAttribute("class","uc-option-image");
       // parentImage[0].appendChild(objectToAppend);
       if (this.use_lightbox)
          $(lightbox).attr('href',images[oid].lightbox);
    }
  }
  catch (e){   
    this.switchImageEffect(image, lightbox, this.noimage); 
  }
};

/**
 * Switch the imagepath based on the selected effect.
 */
UCOI.switchImageEffect = function(image, lightbox, imageproperty) {
  
   if (this.use_lightbox)
      $(lightbox).attr('href',imageproperty.lightbox);

   switch(this.effect){
    case 'fade':
      $(image).fadeOut(200, function(){		      
        $(this).attr('src', imageproperty.derivative).fadeIn(200);
      });
      break;
      
    default:
      $(image).attr('src', imageproperty.derivative); 
  }
};

/**
 * Get attribute AID from an input.
 */
UCOI.getAID = function(input) {
  var name = $(input).attr('name');
  return name.match(/attributes\[([0-9]+)\]/)[1]; 
};

if (Drupal.jsEnabled) {
  $(function(){
    UCOI.init();
  });
}
