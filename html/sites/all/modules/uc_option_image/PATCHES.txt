Ubercart Option Images module
Version: 6.x-1.4

Patches used:
coding_standards_and_cleanup.patch
uc_option_image_capitalization_tweak.patch
uc_option_image_cart_thumbnail.patch

Added Functionality:
Option image shows in cart for each product


Credit: GuyPaddock
Sources:
http://drupal.org/node/550344#comment-2997386
http://drupal.org/node/550344#comment-2997668
http://drupal.org/node/550344#comment-2997684