<?php
// $Id: uc_excel.module,v 1.4 2009/06/10 19:02:18 seanr Exp $

function uc_excel_menu() {
  $items = array();
  $items['admin/store/export'] = array(
    'title' => 'Excel Export',
    'description' => t('Export all orders as a CSV file'),
    'page callback' => 'uc_excel_report_grid',
    'access callback' => 'uc_excel_both_perms',
    'access arguments' => array(''),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/store/export/index'] = array(
    'title' => 'Export Report Settings',
    'description' => t('Set up the reports you want to export.'),
    'page callback' => 'uc_excel_report_grid',
    'access callback' => 'user_access',
    'access arguments' => array('administer excel export settings'),
    'weight' => -25,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/store/export/create'] = array(
    'title' => 'Create New Report',
    'description' => t('Add a new export report.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_excel_add_new_report'),
    'access callback' => 'user_access',
    'access arguments' => array('administer excel export settings'),
    'weight' => -20,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/store/export/execute'] = array(
    'title' => 'Export Report',
    'description' => t('Select report to export.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_excel_select_report_to_export'),
    'access callback' => 'uc_excel_both_perms',
    'access arguments' => array(''),
    'weight' => -15,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/store/export/reports/update/%uc_excel_report_id'] = array(
    'title' => 'Update Report',
    'description' => t('Update an existing report'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_excel_add_new_report',5),
    'access callback' => 'user_access',
    'access arguments' => array('administer excel export settings'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/store/export/reports/delete/%uc_excel_report_id'] = array(
    'title' => 'Delete Report',
    'description' => t('Delete an existing export report.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_excel_delete_report',5),
    'access callback' => 'user_access',
    'access arguments' => array('administer excel export settings'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function uc_excel_both_perms() {
  return user_access('administer excel export settings') || user_access('export ubercart reports as excel csv files');
}

function uc_excel_perm() {
  return array('administer excel export settings', 
               'export ubercart reports as excel csv files', 
  );
}

function uc_excel_report_id_load($id) {
  if (is_numeric($id)) {
    $result = db_query("SELECT * FROM {uc_excel_reports} WHERE rid='%d';",$id);
    if ($result == TRUE) {
      $report = db_fetch_object($result);
    } else {
      drupal_set_message('Unable to load the requested report. Please report this error with a snapshot of your uc_excel_reports table','error');
      return FALSE;
    }
  return $report;
  }
}

function uc_excel_report_grid() {
  $i = 0;
  $rows = array();
  $header = array(
    array('data' => t('Export Name')),
    array('data' => t('Last Exported'), 'style'=>'text-align: center;'),
    array('data' => t('Last Order ID'), 'style'=>'text-align: center;'),
    array('data' => t('Actions'), 'style'=>'text-align: center;'),
  );
  
  $result = db_query('SELECT * FROM {uc_excel_reports} ORDER BY report_name ASC');
  while ($data = db_fetch_object($result)) {
    $row = array();
    $row[] = $data->report_name;    
    $row[] = array('data'=>$data->last_exported,'style'=>'text-align: center;'); 
    $row[] = array('data'=>$data->last_order_id,'style'=>'text-align: center;'); 
    $row[] = array('data'=>l(t('Edit'), 'admin/store/export/reports/update/' . $data->rid) . ' | ' .
             l(t('Delete'), 'admin/store/export/reports/delete/' . $data->rid),'style'=>'text-align: center;'); 
    $rows[] = $row;
  }
  
  if (count($rows) == 0) {
    $rows = array(
      array(
        'data' => array(array('align' => 'center', 'colspan' => 4, 'data' => t('THERE ARE CURRENTLY NO CONFIGURED EXPORT REPORTS')))
      ),
    );
  }
  $output = theme('table', $header, $rows);
  return $output;
}

function uc_excel_select_report_to_export_submit($form,$form_state) {
  $result = db_query("SELECT * FROM {uc_excel_reports} WHERE rid='%d';",$form_state['values']['rid']);
  $report = db_fetch_object($result);
  $selects = array();
  $headers = array( 'order_id','last_name','first_name','email_address','order_total','product_count');
  
  /* cache the list of countries and states in memory so we don't have to do repeated lookups */
  $result = db_query("SELECT country_id,country_name FROM {uc_countries}");
  while($sdata = db_fetch_object($result)) {
    $country[$sdata->country_id] = $sdata->country_name;
  }

  $result = db_query("SELECT zone_id,zone_code FROM {uc_zones}");
  while($sdata = db_fetch_object($result)) {
    $zone[$sdata->zone_id] = $sdata->zone_code;
  }
  
  if ( $report->shipping_address == 1 ) {
    $selects[] = 'o.delivery_first_name,o.delivery_last_name,o.delivery_phone,o.delivery_company,o.delivery_street1,o.delivery_street2,o.delivery_city,o.delivery_zone,o.delivery_postal_code,o.delivery_country';
    $headers = array_merge($headers,array('shipping_first_name','shipping_last_name','shipping_phone','shipping_company','shipping_street1','shipping_street2',
                               'shipping_city','shipping_state','shipping_zipcode','shipping_country'));
  }
  
  if ( $report->billing_address == 1 ) {
    $selects[] = 'o.billing_phone,o.billing_company,o.billing_street1,o.billing_street2,o.billing_city,o.billing_zone,o.billing_postal_code,o.billing_country';
	$headers = array_merge($headers,array('billing_phone','billing_company','billing_street1','billing_street2',
                               'billing_city','billing_state','billing_zipcode','billing_country'));
  }
  
  if ( $report->products == 1 ) {
    $headers = array_merge($headers,array('products'));
  }

  if ( $report->orderby = 'orderid' ) {
    $orderby = 'o.order_id ASC';
  } elseif ( $report->orderby == 'last_name' ) { 
    $orderby = 'o.billing_last_name ASC';
  } else {
    $orderby = 'o.order_id ASC, o.billing_last_name ASC';
  }
  
  if (count($selects) > 0) {
    $sel = ','.join(',',$selects);
  } else {
    $sel = NULL;
  }

  $result = db_query("SELECT o.order_id, o.billing_last_name, o.billing_first_name, o.primary_email,o.order_total, o.product_count".$sel."
                      FROM {uc_orders} o
                      WHERE o.order_id > '%d'
                      ORDER BY ".$orderby,$report->last_order_id);
  while($order = db_fetch_object($result)) {
    $order->delivery_zone = $zone[$order->delivery_zone];
    $order->delivery_country = $country[$order->delivery_country];
    $order->billing_zone = $zone[$order->billing_zone];
    $order->billing_country = $country[$order->billing_country];
  
    if ($report->products == 1) {
      $product = NULL;
      $calc_attribute = array();
      $presults = db_query("SELECT title,qty,price,data FROM {uc_order_products} WHERE order_id='%d'",$order->order_id);
      while($pdata = db_fetch_object($presults)) {
        $data = unserialize($pdata->data);
        if (is_array($data['attributes'])) {
          foreach($data['attributes'] as $key=>$attribute) {
            foreach($attribute as $display_attribute) {
              $calc_attribute[] = $key.": ".$display_attribute; 
            }
          }
        }
        if (count($calc_attribute)>0) {
          $title = $pdata->title . " - ".join(',',$calc_attribute);
        } else {
          $title = $pdata->title;
        }
        $product .= $pdata->qty.' - '.$title.': $'.number_format($pdata->price,2).'ea.';
      }
      $order->products = $product;
    }
    $complete_order[] = (array)$order;
  }
  
  /* get the max order id for storage */
  $result = db_query('SELECT max(order_id) AS order_id FROM {uc_orders}');
  $data = db_fetch_object($result);
  $max_order = $data->order_id;
  
  /* update the table with our latest export time and order_id so we don't re-export things on */
  /* the next go-around */
  $result = db_query("UPDATE {uc_excel_reports} 
                      SET last_order_id='%d', last_exported='%s'
                      WHERE rid='%d';",$max_order,date('Y-m-d H:i:s',time()),$form_state['values']['rid']); 
  
  /* output the file so we download */
  header('Content-Type: text/x-csv'); 
  header('Expires: '. gmdate('D, d M Y H:i:s') .' GMT');
  header('Content-Disposition: inline; filename="'.$_SERVER['SERVER_NAME'].'.csv"');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Pragma: public');
  
  print join(",",$headers)."\n";
  foreach($complete_order as $order) {
    foreach ($order as $cell) {
      $source .= '"'.$cell.'",';
    }
    $source .= "\n";
  }
  echo $source;
}

function uc_excel_add_new_report($form_state,$report) {
  $form = array();
  $form['rid'] = array(
    '#type' => 'hidden',
    '#default_value' => $report->rid,
  );
  $form['report_name'] = array(
    '#type' => 'textfield',
    '#title' => t('List name'),
    '#default_value' => $report->report_name,
    '#max_length' => 96,
    '#required' => TRUE,
  );
  
  $default_options = array();
  if ( $report->shipping_address ) {
    $default_options[] = 'shipping'; 
  }
  if ( $report->billing_address ) {
    $default_options[] = 'billing'; 
  }
  if ( $report->products ) {
    $default_options[] = 'products'; 
  }
  
  
  $form['options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Export Options'),
    '#default_value' => $default_options,
    '#options' => array(
      'shipping' => t('Shipping Address'),
      'billing' => t('Billing Address'),
      'products' => t('Products'),
    ),
	'#description' => t('Select the items you want exported in your report'),
  );
  $form['orderby'] = array(
    '#type' => 'select',
    '#title' => t('Order By'),
    '#default_value' => $report->orderby,
    '#options' => array(
      'order_id' => 'Order ID',
      'last_name' => 'Customer Last Name',
    ),
    '#description' => t('How you would like your CSV report ordered'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#redirect'] = array('admin/store/export/reports');
  return $form;
}

function uc_excel_add_new_report_submit($form,$form_state) {
  $shipping = ( $form_state['values']['options']['shipping'] ) ? 1 : 0;
  $billing = ( $form_state['values']['options']['billing'] ) ? 1 : 0;
  $products = ( $form_state['values']['options']['products'] ) ? 1 : 0;
  
  if ( $form_state['values']['rid'] > 0 ) {
    $array = array(
      'rid' => $form_state['values']['rid'],
      'report_name' => $form_state['values']['report_name'],
      'shipping_address' => $shipping,
      'billing_address' => $billing,
      'products' => $products,
      'orderby' => $form_state['values']['orderby'],
    );
    $result = drupal_write_record('uc_excel_reports',$array,'rid');
  } else {
    $array = array(
      'report_name' => $form_state['values']['report_name'],
      'last_exported' => 0,
      'last_order_id' => 0,
      'shipping_address' => $shipping,
      'billing_address' => $billing,
      'products' => $products,
      'orderby' => $form_state['values']['orderby'],
    );
    $result = drupal_write_record('uc_excel_reports',$array);
  }
  if ($result == TRUE) {
    drupal_set_message("Report successfully saved");
  } else {
    drupal_set_message("Your report could not be saved. Please submit a bug report",'error');
  }
  return;
}

function uc_excel_select_report_to_export($form_state) {
  $result = db_query('SELECT * FROM {uc_excel_reports} ORDER BY report_name ASC;');
  if ($result == TRUE) {
    $options = array();
    while ($ldata = db_fetch_object($result)) {
      $options[$ldata->rid] = $ldata->report_name;
    }
    $form = array();
    $form['rid'] = array (
      '#type' => 'select',
      '#title' => 'Select the report that you would like export',
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['#redirect'] = array('admin/store/export/reports');
    return $form;
  } else {
    drupal_set_message(t('Unable to get list of available reports.'));
    return;
  }
}

function uc_excel_delete_report($form_state, $report) {
  $form = array();
  $form['rid'] = array('#type' => 'hidden', '#value' => $report->rid);
  $form['report_name'] = array('#type' => 'hidden', '#value' => $report->report_name);
  $form['#redirect'] = array('admin/store/export/index');
  return confirm_form( $form,
                       t('Are you sure you wish to delete the %report export report?', array('%report' => $report->report_name)),
                       'admin/store/export/index',
                       t('This action cannot be undone.'),
                       t('Delete'),
                       t('Cancel')
                     );
}

function uc_excel_delete_report_submit($form_state, $form) {
  db_query("DELETE FROM {uc_excel_reports} WHERE rid='%d'", (int) $form['values']['rid']);
  drupal_set_message(t('%report report successfully deleted', array('%report' => $form['values']['report_name'])));
}

