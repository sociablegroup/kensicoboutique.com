<section class="main-banner"> 
	
		<section class="banner-image"> 
			<?php print $node->field_singleimage[0]['view']; ?>
		</section> 
		
		<section class="banner-description"> 
			<h2><?php print $title; ?></h2> 
			<p><?php print $node->content['body']['#value']; ?></p> 
			<?php print $node->field_link[0]['view'];?>
		</section> 
		
	<div class="clearfix"></div> 
		
	</section> <!-- END #main-banner --> 
		