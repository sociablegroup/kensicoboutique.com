<div id="product_full"> 

		<div class="fullimage">
			<a href="/<?php print $node->field_image_cache[0]['filepath'];?>" class = "" rel="position: 'inside' , showTitle: false, adjustX:0, adjustY:0">
				<img src="/sites/default/files/imagecache/product_full/<?php print $node->field_image_cache[0]['filepath']; ?>" />
			</a>
		</div>

		<?php if($node->field_image_cache[1]['filepath'] != "") {  ?>

		<div class="extraviews">
			<?php foreach($node->field_image_cache as $image) {	?>
				<img src="/sites/default/files/imagecache/producttiny/<?php print $image['filepath']; ?>" alt="/sites/default/files/imagecache/product_full/<?php print $image['filepath']; ?>" />
			<?php } //end foreach?>
		</div>
		<?php }//end if ?>
	
		<div class="productdetails">
			<h1><?php print $node->title; ?></h1>
			<div class="description">
				<?php print($node->content['body']['#value']);?>
			</div>
			<div class="price"><?php print "$" . round($node->sell_price, 2) ?></div>
			
			<?php print $node->content['add_to_cart']['#value'] ?>

<div style="display:none;" class="optionimages">
<?php 

$optionimages = $node->option_images;
foreach($optionimages as $optionimage) {
print '<a href="/sites/default/files/imagecache/product_full/'. $optionimage->filepath .'"></a>';
}


?>
</div>			
		</div>
		
<span class="clear"></span>
		
</div>
