		<div class="alternate-product">
		
			<div class="alt-image">
				<?php print $node->field_singleimage[0]['view']; ?>
			</div>
			
			<div class="alt-description">
				<h3><?php print $title; ?></h3>
				<p><?php print $node->content['body']['#value']; ?></p> 
				<?php print $node->field_link[0]['view'];?>
			</div> 
						
		</div> <!-- END .alternate-product -->

