<!doctype html> 
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]--> 
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]--> 
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]--> 
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]--> 
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]--> 
<head> 
	<meta charset="UTF-8"> 
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	
    <title><?php print $head_title; ?></title>
	<meta name="description" content=""> 
	<meta name="author" content=""> 
		
	<!--[if lt IE 9]>
	<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]--> 
	
    <?php print $head; ?>
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <?php print $styles; ?>
    <!--[if lte IE 6]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme() ?>/css/ie6.css"</style><![endif]-->
    <!--[if IE 7]><style type="text/css" media="all">@import "<?php print $base_path . path_to_theme() ?>/css/ie7.css"</style><![endif]-->
    <?php print $scripts; ?>

	
</head> 
<body class="<?php print $body_classes; ?>"> 

	<div id="wrapper"> 

		<header> 
			<div id="header-inner">
				<h1><a href="/"><?php print $site_name; ?></a></h1> 


				<nav class="main-nav"> 
					<ul id="primary" class="links main-menu">
					<li class="menu-795 first"><a href="/shop/" title="">Collection</a></li>
					<li class="menu-about"><a href="/about" title="About">Story</a></li>
					<li class="menu-contact"><a href="/contact" title="Contact">Contact</a></li>
					</ul>
	 			</nav> 

				<nav class="secondary-nav"> 
					<ul id="secondary" class="links">
					<li class="menu-795 first"><a href="/cart" title="">Cart</a></li>
					<li class="menu-about"><a href="/user" title="About" data-content="about_section">Account</a></li>
					</ul>
	 			</nav> 


  <?php if ($header): ?>
        <div id="header-region">
          <?php print $header; ?>
        </div>
      <?php endif; ?>
			
			</div><!-- /header-inner -->		
		</header> 
 
		<section id="content"> 
	
		     <div id="content-inner" class="inner column center">
		
		          <?php if ($content_top): ?>
		            <div id="content-top">
		              <?php print $content_top; ?>
		            </div> <!-- /#content-top -->
		          <?php endif; ?>
		
		          <?php if ($breadcrumb || $title || $mission || $messages || $help || $tabs): ?>
		            <div id="content-header">
		              <?php if ($tabs): ?>
		                <div class="tabs"><?php print $tabs; ?></div>
		              <?php endif; ?>
		
		
		              <?php print $breadcrumb; ?>
		
		              <?php if ($title): ?>
		                <h1 class="title"><?php print $title; ?></h1>
		              <?php endif; ?>
		
		              <?php if ($mission): ?>
		                <div id="mission"><?php print $mission; ?></div>
		              <?php endif; ?>
		
		              <?php print $messages; ?>
		
		              <?php print $help; ?> 
		
		            </div> <!-- /#content-header -->
		          <?php endif; ?>
		
		          <div id="content-area">
		          	<?php if($node->field_image_cache[0]['filepath'] != '') { ?>
		          		<!-- <div class="pageimage"><img src="<?php print $node->field_image_cache[0]['filepath']; ?>" /></div> -->
		          	<?php } ?>
		            <?php print $content; ?>
		          </div> <!-- /#content-area -->
		
		          <?php print $feed_icons; ?>
		
		          <?php if ($content_bottom): ?>
		            <div id="content-bottom">
		              <?php print $content_bottom; ?>
		            </div><!-- /#content-bottom -->
		          <?php endif; ?>
		

		        </div> <!-- /content-inner /content -->
		
						<span class="clear"></span>
		</section> <!-- END #content --> 
			
			
			</div> <!-- END #wrapper --> 
			
			
			
			
			<footer>
				<div id="footer-inner">
					<nav id="social">
						<ul class="links">
							<li class="facebook"><a href="http://www.facebook.com/pages/Kensico-Boutique/281037761919425">Facebook</a></li>
							<!-- <li class="twitter"><a href="http://twitter.com/kensicoboutique">Twitter</a></li> -->
						</ul>
					</nav>

	
					<nav id="footer-nav">
						<ul class="links">
							<li class="last"><a href="/user" title="About">Contact</a></li>
						</ul>
					</nav>	

					<div id="copyright">All content &copy;<?php print date('Y'); ?> Kensico Boutique. Design and Marketing by <a href="http://excela.com">excela creative</a></div>			

				</div><!-- /footer-inner -->
			</footer>
			
			
			    <?php print $closure; ?>
				<!--[if lt IE 7 ]>
				<script src="js/libs/dd_belatedpng.js"></script>
				<script> DD_belatedPNG.fix('img, .png_bg');</script>
				<![endif]--> 
			</body> 
			</html>




