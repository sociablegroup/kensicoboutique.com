var toshow = "";
$(document).ready(function() {

	$(".main-nav .menu-about a, .main-nav .menu-contact a").click(function() {
		toshow = $(this).attr("data-content");
		if( $("#slide_content:visible").length) {
			$("#slide_content").slideUp(300, function() {
				showSection(toshow);
			});
		} else {
			showSection(toshow);		
		}
	});
	
	if( $("body.front").length ) {
/* 		$(".content div").cycle(); */
	}
	
	if($(".view-id-Products").length) {

		$(".view-id-Products .views-row").click(function() {
			window.location.href = $(this).find("a").attr("href");
		});
	}
	
	 


	if($("#product_full").length) {
	

		$(".attributes select").change(function() {
			var selectedIndex = $(this).attr("selectedIndex");
			selectedIndex = selectedIndex - 1;
			var filename = $(this).parents("#product_full").find(".optionimages a:eq("+ selectedIndex +")").attr("href");
			$(this).parents("#product_full").find(".fullimage img").attr("src", filename);
		});

	
	}

	if($('.extraviews').length) {
	
	}
	
	if($("body.page-collection").length) {
		$("div.collection li").click(function() {
			window.location.href = $(this).find("a").attr("href");
		});
	}

}); /* end doc ready */


function showSection(contentSelector) {
	$("#slide_content").empty();
	$("#"+contentSelector).clone().addClass("slide-inner").appendTo("#slide_content");
	$("<div />").addClass("button-close").appendTo("#slide_content");
	$("#slide_content").slideDown(300);
	$(".button-close").click(function() {
		$("#slide_content").slideUp(300);	
	});
}